Python Boilerplate
==================

Stuff used for a new python project 🐍


💡 Idea
-------

1. Keep in mind the **Standalone Python script** proposed by _Vincent Bernat_ ( [🇫🇷](https://vincent.bernat.ch/fr/blog/2019-script-python-durable) | [🇬🇧](https://vincent.bernat.ch/en/blog/2019-sustainable-python-script) ).
1. Ease new project start
1. Give a try to the _GitLab_ group organisation


🚀 Use quick-start
-----------------

Get the source & edit settings:
```bash
git clone git@<GIT REPO URL>:<GIT REPO PATH>.git && cd <GIT REPO NAME>
cp client.sample.py client.py
${EDITOR} client.py
```

Set up environment & run help:
```bash
mkdir ~/.venvs && python3 -m venv ~/.venvs/<GIT REPO NAME>
source ~/.venvs/<GIT REPO NAME>/bin/activate
pip install -r requirements.txt
./main.py -h
```


🚧 Development
--------------


### 🚀 Quick-start

- Built with `Python 3.8`
- Code linting with [`flake8`](https://pypi.org/project/flake8/), [`pylint`](https://pypi.org/project/pylint), [`black`](https://pypi.org/project/black) & [`pydocstyle`](https://pypi.org/project/pydocstyle/).
- Install development tools:
    * `pip install -r requirements-dev.txt`
- A `Makefile` with tools : run `make help` to have a look
- Use the `pre-commit` git hook
    * `make init-pre-commit`


### 📌 Dependences

Details in [`requirements.txt`](requirements.txt) & [`requirements-dev.txt`](requirements-dev.txt)


### 🤝 Contributing

- Roadmap ➡️ [_project kanban_](https://gitlab.com/forga/devel/boilerplate/python/-/boards)
- Question, suggestion, issue ➡️ [_project issues_](https://gitlab.com/forga/devel/boilerplate/python/-/issues/new)
- Code submission  ➡️ [_project merge request_](https://gitlab.com/forga/devel/boilerplate/python/-/merge_requests/new)
